import java.util.Arrays;

public class Matrix {

    int[][] matrix;

    public Matrix(int[][] newMatrix) {
        this.matrix = newMatrix;
    }

    public int getFirstElement() {
        return matrix[0][0];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Matrix matrix1 = (Matrix) o;
        return Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(matrix);
    }

    public int highestValue() {
        throw new UnsupportedOperationException();
    }

    public Matrix multiplyBy(int value) {
        throw new UnsupportedOperationException();
    }
}
