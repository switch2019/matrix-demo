import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MatrixTest {

    @Test
    public void ensureFirstElementIsOne() {
        //Arrange
        int expectedResult = 1;
        int realResult = -1;
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix matrix = new Matrix(defaultMatrix);

        realResult = matrix.getFirstElement();

        //Assert
        assertEquals(expectedResult, realResult);
    }

    @Test
    public void ensureTwoMatrixAreEqual() {
        //Arrange
        int[][] defaultMatrix = {{1, 2}, {3, 4}};

        //Act
        Matrix firstMatrix = new Matrix(defaultMatrix);

        Matrix secondMatrix = new Matrix(defaultMatrix);

        assertEquals(firstMatrix, secondMatrix);
    }
}